import React,{useState,useEffect} from 'react';
import User from './User';
import axios from 'axios';

const UserList = () => {

    const [userList, setUserList] = useState([]);

    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com/users`)
            .then(res => {
                setUserList(res.data);
            })
    }, []);

    console.log(userList);

    return (
        <div className='userList'>
            {userList.map(user => (
                <User data={user} key={user.id}/>
            ))}
        </div>
    );
}

export default UserList;

