import React from 'react';
import './user.css';

const User = ({data}) => {
    return (
        <div className='user'>
            <h2>{data.name}</h2>
            <p>{data.email}</p>
            <p>{data.username}</p>
            <address>{data.address.city}</address>
            <p>{data.company.name}</p>
            <p>{data.phone}</p>
            <p><a href= {`https://www.${data.website}`}>{data.website}</a></p>

        </div>
    );
}

export default User;
